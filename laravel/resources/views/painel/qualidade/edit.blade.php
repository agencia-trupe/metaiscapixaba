@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Qualidade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.qualidade.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.qualidade.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
