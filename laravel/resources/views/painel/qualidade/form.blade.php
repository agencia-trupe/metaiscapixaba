@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_1', 'Texto 1') !!}
    {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_2', 'Texto 2') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem 1') !!}
    @if($registro->imagem_1)
    <img src="{{ url('assets/img/qualidade/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_2', 'Imagem 2') !!}
    @if($registro->imagem_2)
    <img src="{{ url('assets/img/qualidade/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_3', 'Imagem 3') !!}
    @if($registro->imagem_3)
    <img src="{{ url('assets/img/qualidade/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_4', 'Imagem 4') !!}
    @if($registro->imagem_4)
    <img src="{{ url('assets/img/qualidade/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
