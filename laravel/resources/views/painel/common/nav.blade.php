<ul class="nav navbar-nav">
    <li class="dropdown @if(Tools::routeIs([
        'painel.home.index',
        'painel.banners*',
        'painel.home-imagens*',
        'painel.chamadas*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.home.index')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Home</a>
            </li>
            <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(Tools::routeIs('painel.home-imagens*')) class="active" @endif>
                <a href="{{ route('painel.home-imagens.index') }}">Imagens</a>
            </li>
            <li @if(Tools::routeIs('painel.chamadas*')) class="active" @endif>
                <a href="{{ route('painel.chamadas.index') }}">Chamadas</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Tools::routeIs([
        'painel.empresa.index',
        'painel.linha-do-tempo*',
        'painel.empresa-certificacoes*',
        'painel.empresa-arquivos*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Empresa
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.empresa.index')) class="active" @endif>
                <a href="{{ route('painel.empresa.index') }}">Empresa</a>
            </li>
            <li @if(Tools::routeIs('painel.linha-do-tempo*')) class="active" @endif>
                <a href="{{ route('painel.linha-do-tempo.index') }}">Linha do Tempo</a>
            </li>
            <li @if(Tools::routeIs('painel.empresa-certificacoes*')) class="active" @endif>
                <a href="{{ route('painel.empresa-certificacoes.index') }}">Certificações</a>
            </li>
            <li @if(Tools::routeIs('painel.empresa-arquivos*')) class="active" @endif>
                <a href="{{ route('painel.empresa-arquivos.index') }}">Arquivos</a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.produtos*')) class="active" @endif>
        <a href="{{ route('painel.produtos.index') }}">Produtos</a>
    </li>
    <li class="dropdown @if(Tools::routeIs([
        'painel.qualidade.index',
        'painel.qualidade-arquivos*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Qualidade
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.qualidade.index')) class="active" @endif>
                <a href="{{ route('painel.qualidade.index') }}">Qualidade</a>
            </li>
            <li @if(Tools::routeIs('painel.qualidade-arquivos*')) class="active" @endif>
                <a href="{{ route('painel.qualidade-arquivos.index') }}">Arquivos</a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.sustentabilidade*')) class="active" @endif>
        <a href="{{ route('painel.sustentabilidade.index') }}">Sustentabilidade</a>
    </li>
    <li @if(Tools::routeIs('painel.seja-nosso-fornecedor*')) class="active" @endif>
        <a href="{{ route('painel.seja-nosso-fornecedor.index') }}">Seja nosso Fornecedor</a>
    </li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>
