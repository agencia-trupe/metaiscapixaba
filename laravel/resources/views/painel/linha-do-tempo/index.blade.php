@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Linha do Tempo
            <a href="{{ route('painel.linha-do-tempo.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Registro</a>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Ano</th>
                <th>Descrição</th>
                <th>Imagem</th>
                <th>Destaque</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->ano }}</td>
                <td>{!! $registro->descricao !!}</td>
                <td><img src="{{ asset('assets/img/linha-do-tempo/'.$registro->imagem) }}" style="width: 100%; max-width:100px;" alt=""></td>
                <td>{!! $registro->destaque ? '<span class="glyphicon glyphicon-ok"></span>' : '' !!}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.linha-do-tempo.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.linha-do-tempo.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
