@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha do Tempo /</small> Editar Registro</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.linha-do-tempo.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.linha-do-tempo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
