@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linha do Tempo /</small> Adicionar Registro</h2>
    </legend>

    {!! Form::open(['route' => 'painel.linha-do-tempo.store', 'files' => true]) !!}

        @include('painel.linha-do-tempo.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
