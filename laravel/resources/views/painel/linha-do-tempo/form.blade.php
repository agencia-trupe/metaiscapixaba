@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('ano', 'Ano') !!}
    {!! Form::text('ano', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (opcional, se preenchida sobrepõe a descrição)') !!}
@if($submitText == 'Alterar' && $registro->imagem)
    <div>
        <a href="{{ route('painel.linha-do-tempo.deleteImage', $registro->id) }}" class="btn btn-sm btn-danger btn-delete btn-delete-link">
            <span class="glyphicon glyphicon-trash" style="margin-right:10px"></span>
            Remover imagem
        </a>
    </div>
    <img src="{{ url('assets/img/linha-do-tempo/'.$registro->imagem) }}" style="display:block; margin: 10px 0; width: 100%; max-width: 260px">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    <label style="margin-bottom:0;display:flex;align-items:center;cursor:pointer">
        {!! Form::hidden('destaque', 0) !!}
        {!! Form::checkbox('destaque', 1, null, ['style' => 'margin:0']) !!}
        <strong style="display:inline-block;margin-left:5px;">Destaque (ocupa duas colunas)</strong>
    </label>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.linha-do-tempo.index') }}" class="btn btn-default btn-voltar">Voltar</a>
