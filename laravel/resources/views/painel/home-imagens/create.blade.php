@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Imagens /</small> Adicionar Imagem</h2>
    </legend>

    {!! Form::open(['route' => 'painel.home-imagens.store', 'files' => true]) !!}

        @include('painel.home-imagens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
