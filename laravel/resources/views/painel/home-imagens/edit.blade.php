@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Imagens /</small> Editar Imagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.home-imagens.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.home-imagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
