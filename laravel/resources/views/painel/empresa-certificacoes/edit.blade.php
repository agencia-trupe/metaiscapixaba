@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Certificações /</small> Editar Certificação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.empresa-certificacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.empresa-certificacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
