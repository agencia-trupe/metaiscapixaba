@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::textarea('frase', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('frase_sustentabilidade', 'Frase Sustentabilidade') !!}
    {!! Form::text('frase_sustentabilidade', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('background_sustentabilidade', 'Background Sustentabilidade') !!}
    @if($registro->background_sustentabilidade)
    <img src="{{ url('assets/img/home/'.$registro->background_sustentabilidade) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('background_sustentabilidade', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
