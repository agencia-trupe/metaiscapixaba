@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Seja nosso Fornecedor</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.seja-nosso-fornecedor.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.seja-nosso-fornecedor.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
