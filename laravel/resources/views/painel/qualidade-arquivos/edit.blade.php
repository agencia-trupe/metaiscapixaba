@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Arquivos /</small> Editar Arquivo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.qualidade-arquivos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.qualidade-arquivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
