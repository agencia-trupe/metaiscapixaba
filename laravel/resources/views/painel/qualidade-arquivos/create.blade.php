@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Arquivos /</small> Adicionar Arquivo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.qualidade-arquivos.store', 'files' => true]) !!}

        @include('painel.qualidade-arquivos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
