@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Arquivos /</small> Editar Arquivo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.empresa-arquivos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.empresa-arquivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
