@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Sustentabilidade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.sustentabilidade.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.sustentabilidade.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
