@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <h1>Produtos</h1>
            <div class="produtos-content">
                <h2>{{ $produtos->titulo }}</h2>
                <div class="texto">{!! $produtos->texto_1 !!}</div>
                <div class="box">
                    <div class="texto">{!! $produtos->texto_2 !!}</div>
                </div>
            </div>
        </div>

        <div class="imagens">
            <div class="center">
                <img src="{{ asset('assets/img/produtos/'.$produtos->imagem_1) }}" alt="">
                <div>
                    <img src="{{ asset('assets/img/produtos/'.$produtos->imagem_2) }}" alt="">
                    <img src="{{ asset('assets/img/produtos/'.$produtos->imagem_3) }}" alt="">
                </div>
                <img src="{{ asset('assets/img/produtos/'.$produtos->imagem_4) }}" alt="">
            </div>
        </div>
    </div>

@endsection
