    <footer>
        <div class="center">
            <div class="col links">
                <a href="{{ route('home') }}">Home</a>
                <a href="{{ route('empresa') }}">Empresa</a>
                <a href="{{ route('produtos') }}">Produtos</a>
                <a href="{{ route('qualidade') }}">Qualidade</a>
                <a href="{{ route('sustentabilidade') }}">Sustentabilidade</a>
                <a href="{{ route('fornecedor') }}">Seja nosso Fornecedor</a>
                <a href="{{ route('contato') }}">Contato</a>
            </div>
            <div class="col informacoes">
                <img src="{{ asset('assets/img/layout/marca-metaiscapixaba-rodape.png') }}" alt="">
                <div class="telefones">
                    {{ str_replace(',', ' &middot; ', trim($contato->telefones)) }}
                </div>
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>
            <div class="col parceiro">
                <h4>SEJA NOSSO PARCEIRO</h4>
                <p>
                    {{ $fornecedor->titulo }}<br>
                    {{ $fornecedor->frase }}
                </p>
                <div class="telefones">
                    @foreach(explode(',', trim($contato->telefones)) as $telefone)
                    <span>{{ trim($telefone) }}</span>
                    @endforeach
                </div>
            </div>
            <div class="col copyright">
                <p>© {{ date('Y') }} {{ config('app.name') }}<br>Todos os direitos reservados.</p>
                <p>
                    <a href="http://www.trupe.net">Criação de sites</a>:<br>
                    <a href="http://www.trupe.net">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
