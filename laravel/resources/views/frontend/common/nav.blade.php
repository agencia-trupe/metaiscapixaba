<a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa')) class="active" @endif>
    Empresa
</a>
<a href="{{ route('produtos') }}" @if(Tools::routeIs('produtos')) class="active" @endif>
    Produtos
</a>
<a href="{{ route('qualidade') }}" @if(Tools::routeIs('qualidade')) class="active" @endif>
    Qualidade
</a>
<a href="{{ route('sustentabilidade') }}" @if(Tools::routeIs('sustentabilidade')) class="active" @endif>
    Sustentabilidade
</a>
<a href="{{ route('fornecedor') }}" @if(Tools::routeIs('fornecedor')) class="active" @endif>
    Seja nosso Fornecedor
</a>
<a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
    Contato
</a>
