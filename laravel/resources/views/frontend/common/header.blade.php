    <header @if(Tools::routeIs('home')) class="home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
            <nav id="nav-desktop">
                @include('frontend.common.nav')
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <div id="nav-mobile">
            @include('frontend.common.nav')
        </div>
    </header>
