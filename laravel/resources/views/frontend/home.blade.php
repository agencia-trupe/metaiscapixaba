@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
                <div class="center">
                    <div class="texto">{!! $banner->texto !!}</div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="abertura">
            <div class="center">
                <h3>{!! $home->frase !!}</h3>
                <div class="imagens">
                    @foreach($imagens as $imagem)
                    <img src="{{ asset('assets/img/home-imagens/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>

        <div class="chamadas">
            <div class="center">
                @foreach($chamadas as $chamada)
                    @if($chamada->link)
                    <a href="{{ Tools::parseLink($chamada->link) }}">
                        <img src="{{ asset('assets/img/chamadas/'.$chamada->imagem) }}" alt="">
                        <div class="overlay">
                            <h4>{{ $chamada->titulo }}</h4>
                            <p>{{ $chamada->descricao }}</p>
                        </div>
                    </a>
                    @else
                    <div>
                        <img src="{{ asset('assets/img/chamadas/'.$chamada->imagem) }}" alt="">
                        <div class="overlay">
                            <h4>{{ $chamada->titulo }}</h4>
                            <p>{{ $chamada->descricao }}</p>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>

        <a href="{{ route('sustentabilidade') }}" class="sustentabilidade" style="background-image:url({{ asset('assets/img/home/'.$home->background_sustentabilidade) }}">
            <div class="center">
                <div class="texto">
                    <span>Sustentabilidade</span>
                    <span>{{ $home->frase_sustentabilidade }}</span>
                </div>
                <span>saiba mais &raquo;</span>
            </div>
        </a>
    </div>

@endsection
