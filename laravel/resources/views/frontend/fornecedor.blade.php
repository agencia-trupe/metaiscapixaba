@extends('frontend.common.template')

@section('content')

    <div class="main fornecedor">
        <div class="center">
            <h1>Seja nosso fornecedor</h1>
            <div class="fornecedor-content">
                <h2>{{ $fornecedor->titulo }}</h2>
                <img src="{{ asset('assets/img/seja-nosso-fornecedor/'.$fornecedor->imagem) }}" alt="">
                <div class="texto">
                    <p>Entre em contato pelos telefones:</p>
                    <div class="telefones">
                        @foreach(explode(',', trim($contato->telefones)) as $telefone)
                        <span>
                            <img src="{{ asset('assets/img/layout/icone-telefone.png') }}" alt="">
                            {{ trim($telefone) }}
                        </span>
                        @endforeach
                    </div>
                    <p class="frase">{{ $fornecedor->frase }}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
