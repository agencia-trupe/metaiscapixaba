@extends('frontend.common.template')

@section('content')

    <div class="main qualidade">
        <div class="center">
            <h1>Qualidade</h1>

            <div class="qualidade-content-1">
                <h2>{{ $qualidade->titulo }}</h2>
                <div class="texto">{!! $qualidade->texto_1 !!}</div>
                <img src="{{ asset('assets/img/qualidade/'.$qualidade->imagem_1) }}" alt="">
            </div>

            <div class="qualidade-content-2">
                <div class="imagens">
                    <img src="{{ asset('assets/img/qualidade/'.$qualidade->imagem_2) }}" alt="">
                    <img src="{{ asset('assets/img/qualidade/'.$qualidade->imagem_3) }}" alt="">
                </div>
                <div class="texto">{!! $qualidade->texto_2 !!}</div>
            </div>

            <div class="arquivos">
                <img src="{{ asset('assets/img/qualidade/'.$qualidade->imagem_4) }}" alt="">
                @foreach($arquivos as $arquivo)
                <a href="{{ asset('assets/qualidade-arquivos/'.$arquivo->arquivo) }}">
                    <img src="{{ asset('assets/img/qualidade-arquivos/'.$arquivo->capa) }}" alt="">
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
