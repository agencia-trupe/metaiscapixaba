@extends('frontend.common.template')

@section('content')

    <div class="main sustentabilidade">
        <div class="center">
            <h1>Sustentabilidade</h1>
            <div class="sustentabilidade-content">
                <h2>{{ $sustentabilidade->titulo }}</h2>
                <img src="{{ asset('assets/img/sustentabilidade/'.$sustentabilidade->imagem_1) }}" alt="">
                <div class="texto">{!! $sustentabilidade->texto !!}</div>
            </div>
        </div>

        <div class="imagens">
            <div class="center">
                <img src="{{ asset('assets/img/sustentabilidade/'.$sustentabilidade->imagem_2) }}" alt="">
                <img src="{{ asset('assets/img/sustentabilidade/'.$sustentabilidade->imagem_3) }}" alt="">
                <img src="{{ asset('assets/img/sustentabilidade/'.$sustentabilidade->imagem_4) }}" alt="">
            </div>
        </div>
    </div>

@endsection
