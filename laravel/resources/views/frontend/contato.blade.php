@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <div class="left">
                <h1>Contato</h1>
                <div class="telefones">
                    @foreach(explode(',', trim($contato->telefones)) as $telefone)
                    <span>
                        <img src="{{ asset('assets/img/layout/icone-telefone.png') }}" alt="">
                        {{ trim($telefone) }}
                    </span>
                    @endforeach
                </div>
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>

            <div class="right">
                <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
                <form action="{{ route('contato.post') }}" method="POST">
                    <h2>
                        Fale conosco<br>
                        Peça sua cotação
                    </h2>

                    {!! csrf_field() !!}

                    <div class="wrapper">
                        <div class="col">
                            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                            <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                            <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                        </div>
                        <input type="submit" value="">
                    </div>

                    @if(session('enviado'))
                    <div class="flash flash-success">
                        Mensagem enviada com sucesso!
                    </div>
                    @endif
                    @if($errors->any())
                    <div class="flash flash-error">
                        Preencha todos os campos corretamente.
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

@endsection
