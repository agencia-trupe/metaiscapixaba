@extends('frontend.common.template')

@section('content')

    <div class="main empresa">
        <div class="center">
            <h1>Empresa</h1>
            <h3>{{ $empresa->titulo }}</h3>

            <div class="linha-do-tempo">
                @foreach($linhaDoTempo as $registro)
                <div class="registro{{ $registro->destaque ? ' destaque' : ''}}">
                    @if($registro->imagem)
                        <img src="{{ asset('assets/img/linha-do-tempo/'.$registro->imagem) }}" alt="">
                        <div class="imagem-full" style="background-image:url({{ asset('assets/img/linha-do-tempo/'.$registro->imagem) }}"></div>
                    @else
                        <p class="ano">{{ $registro->ano }}</p>
                        <p class="descricao">{!! $registro->descricao !!}</p>
                    @endif
                </div>
                @endforeach
            </div>

            <div class="certificacoes">
                @foreach($certificacoes as $certificacao)
                <div class="certificacao">
                    <img src="{{ asset('assets/img/empresa-certificacoes/'.$certificacao->imagem) }}" alt="">
                    <div>
                        <p class="titulo">{{ $certificacao->titulo }}</p>
                        <p class="descricao">{!! $certificacao->descricao !!}</p>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="textos">
                <div class="texto texto-1">{!! $empresa->texto !!}</div>
                <div class="texto texto-2">
                    @foreach([
                        'missao'  => 'Missão',
                        'visao'   => 'Visão',
                        'valores' => 'Valores'
                    ] as $slug => $titulo)
                    <div class="topico">
                        <img src="{{ asset('assets/img/layout/icone-'.$slug.'.png') }}" alt="">
                        <div>
                            <p class="titulo">{{ $titulo }}</p>
                            <p class="descricao">{{ $empresa->{$slug} }}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="arquivos">
                @foreach($arquivos as $arquivo)
                <a href="{{ asset('assets/empresa-arquivos/'.$arquivo->arquivo) }}" target="_blank">
                    <img src="{{ asset('assets/img/empresa-arquivos/'.$arquivo->capa) }}" alt="">
                    <div>
                        <p class="titulo">{{ $arquivo->titulo }}</p>
                        <p class="descricao">{{ $arquivo->descricao }}</p>
                        <span class="icone"></span>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
