const config = {
    padrao: {
        toolbar: [['Bold', 'Italic'], ['BulletedList']],
    },

    clean: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
    },

    cleanBr: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
        enterMode: CKEDITOR.ENTER_BR,
    },

    banner: {
        toolbar: [['Bold', 'Italic']],
        removePlugins: 'toolbar,elementspath',
        enterMode: CKEDITOR.ENTER_BR,
    },

    empresa: {
        toolbar: [['Format'], ['Bold', 'Italic'], ['BulletedList']],
        format_tags: 'h3;p',
    },
};

export default function TextEditor() {
    CKEDITOR.config.language = 'pt-br';
    CKEDITOR.config.uiColor = '#dce4ec';
    CKEDITOR.config.contentsCss = [
        `${$('base').attr('href')}/assets/ckeditor.css`,
        CKEDITOR.config.contentsCss,
    ];
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.resize_enabled = false;
    CKEDITOR.plugins.addExternal('injectimage', `${$('base').attr('href')}/assets/injectimage/plugin.js`);
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraPlugins = 'injectimage';

    $('.ckeditor').each((i, obj) => {
        CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
    });
}
