<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualidadeArquivosTable extends Migration
{
    public function up()
    {
        Schema::create('qualidade_arquivos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('capa');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('qualidade_arquivos');
    }
}
