<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSejaNossoFornecedorTable extends Migration
{
    public function up()
    {
        Schema::create('seja_nosso_fornecedor', function (Blueprint $table) {
            $table->increments('id');
            $table->text('titulo');
            $table->text('frase');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('seja_nosso_fornecedor');
    }
}
