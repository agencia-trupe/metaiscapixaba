<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinhaDoTempoTable extends Migration
{
    public function up()
    {
        Schema::create('linha_do_tempo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ano');
            $table->text('descricao');
            $table->string('imagem');
            $table->boolean('destaque')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('linha_do_tempo');
    }
}
