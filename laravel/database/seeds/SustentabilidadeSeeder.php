<?php

use Illuminate\Database\Seeder;

class SustentabilidadeSeeder extends Seeder
{
    public function run()
    {
        DB::table('sustentabilidade')->insert([
            'titulo' => '',
            'texto' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
        ]);
    }
}
