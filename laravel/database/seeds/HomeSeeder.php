<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'frase' => '',
            'frase_sustentabilidade' => '',
            'background_sustentabilidade' => '',
        ]);
    }
}
