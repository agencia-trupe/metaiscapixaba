<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(EmpresaSeeder::class);
		$this->call(QualidadeSeeder::class);
		$this->call(HomeSeeder::class);
		$this->call(SejaNossoFornecedorSeeder::class);
		$this->call(SustentabilidadeSeeder::class);
		$this->call(ProdutosSeeder::class);
		$this->call(ConfiguracoesSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
