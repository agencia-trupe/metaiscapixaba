<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Produtos extends Model
{
    protected $table = 'produtos';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 395,
            'height' => 480,
            'path'   => 'assets/img/produtos/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 395,
            'height' => 235,
            'path'   => 'assets/img/produtos/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 395,
            'height' => 235,
            'path'   => 'assets/img/produtos/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 395,
            'height' => 480,
            'path'   => 'assets/img/produtos/'
        ]);
    }

}
