<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class QualidadeArquivo extends Model
{
    protected $table = 'qualidade_arquivos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 180,
            'height' => 250,
            'path'   => 'assets/img/qualidade-arquivos/'
        ]);
    }
}
