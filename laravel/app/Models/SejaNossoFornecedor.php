<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class SejaNossoFornecedor extends Model
{
    protected $table = 'seja_nosso_fornecedor';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 430,
            'height' => 250,
            'path'   => 'assets/img/seja-nosso-fornecedor/'
        ]);
    }

}
