<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Qualidade extends Model
{
    protected $table = 'qualidade';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 430,
            'height' => 280,
            'path'   => 'assets/img/qualidade/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 210,
            'height' => 280,
            'path'   => 'assets/img/qualidade/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 210,
            'height' => 280,
            'path'   => 'assets/img/qualidade/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 690,
            'height' => 250,
            'path'   => 'assets/img/qualidade/'
        ]);
    }

}
