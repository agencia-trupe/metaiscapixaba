<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class EmpresaArquivo extends Model
{
    protected $table = 'empresa_arquivos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 150,
            'height' => 150,
            'path'   => 'assets/img/empresa-arquivos/'
        ]);
    }
}
