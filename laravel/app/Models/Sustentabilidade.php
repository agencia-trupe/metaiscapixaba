<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Sustentabilidade extends Model
{
    protected $table = 'sustentabilidade';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 430,
            'height' => 250,
            'path'   => 'assets/img/sustentabilidade/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 295,
            'height' => 295,
            'path'   => 'assets/img/sustentabilidade/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 295,
            'height' => 295,
            'path'   => 'assets/img/sustentabilidade/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 594,
            'height' => 295,
            'path'   => 'assets/img/sustentabilidade/'
        ]);
    }

}
