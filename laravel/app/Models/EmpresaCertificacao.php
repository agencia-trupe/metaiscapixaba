<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class EmpresaCertificacao extends Model
{
    protected $table = 'empresa_certificacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 235,
            'height' => null,
            'path'   => 'assets/img/empresa-certificacoes/'
        ]);
    }
}
