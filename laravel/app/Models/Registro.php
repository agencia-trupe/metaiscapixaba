<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Registro extends Model
{
    protected $table = 'linha_do_tempo';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ano', 'ASC')->orderBy('id', 'ASC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 600,
            'height' => 500,
            'path'   => 'assets/img/linha-do-tempo/'
        ]);
    }
}
