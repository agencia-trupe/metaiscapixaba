<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_background_sustentabilidade()
    {
        return CropImage::make('background_sustentabilidade', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/home/'
        ]);
    }

}
