<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('linha-do-tempo', 'App\Models\Registro');
		$router->model('empresa-certificacoes', 'App\Models\EmpresaCertificacao');
		$router->model('empresa-arquivos', 'App\Models\EmpresaArquivo');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('qualidade-arquivos', 'App\Models\QualidadeArquivo');
		$router->model('qualidade', 'App\Models\Qualidade');
		$router->model('home-imagens', 'App\Models\HomeImagem');
		$router->model('chamadas', 'App\Models\Chamada');
		$router->model('banners', 'App\Models\Banner');
		$router->model('home', 'App\Models\Home');
		$router->model('seja-nosso-fornecedor', 'App\Models\SejaNossoFornecedor');
		$router->model('sustentabilidade', 'App\Models\Sustentabilidade');
		$router->model('produtos', 'App\Models\Produtos');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
