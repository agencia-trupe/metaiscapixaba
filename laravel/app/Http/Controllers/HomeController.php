<?php

namespace App\Http\Controllers;

use App\Models\Home;
use App\Models\Banner;
use App\Models\HomeImagem;
use App\Models\Chamada;

class HomeController extends Controller
{
    public function index()
    {
        $home = Home::first();
        $banners = Banner::ordenados()->get();
        $imagens = HomeImagem::ordenados()->get();
        $chamadas = Chamada::ordenados()->get();

        return view('frontend.home', compact('home', 'banners', 'imagens', 'chamadas'));
    }
}
