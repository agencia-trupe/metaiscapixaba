<?php

namespace App\Http\Controllers;

use App\Models\Sustentabilidade;

class SustentabilidadeController extends Controller
{
    public function index()
    {
        $sustentabilidade = Sustentabilidade::first();

        return view('frontend.sustentabilidade', compact('sustentabilidade'));
    }
}
