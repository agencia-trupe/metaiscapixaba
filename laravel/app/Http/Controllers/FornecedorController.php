<?php

namespace App\Http\Controllers;

use App\Models\Contato;
use App\Models\SejaNossoFornecedor;

class FornecedorController extends Controller
{
    public function index()
    {
        $contato = Contato::first();
        $fornecedor = SejaNossoFornecedor::first();

        return view('frontend.fornecedor', compact(
            'contato', 'fornecedor'
        ));
    }
}
