<?php

namespace App\Http\Controllers;

use App\Models\Qualidade;
use App\Models\QualidadeArquivo;

class QualidadeController extends Controller
{
    public function index()
    {
        $qualidade = Qualidade::first();
        $arquivos = QualidadeArquivo::ordenados()->get();

        return view('frontend.qualidade', compact('qualidade', 'arquivos'));
    }
}
