<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\Registro;
use App\Models\EmpresaCertificacao;
use App\Models\EmpresaArquivo;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();
        $linhaDoTempo = Registro::ordenados()->get();
        $certificacoes = EmpresaCertificacao::ordenados()->get();
        $arquivos = EmpresaArquivo::ordenados()->get();

        return view('frontend.empresa', compact(
            'empresa',
            'linhaDoTempo',
            'certificacoes',
            'arquivos'
        ));
    }
}
