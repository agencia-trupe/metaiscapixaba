<?php

namespace App\Http\Controllers;

use App\Models\Produtos;

class ProdutosController extends Controller
{
    public function index()
    {
        $produtos = Produtos::first();

        return view('frontend.produtos', compact('produtos'));
    }
}
