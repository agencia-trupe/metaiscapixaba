<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SustentabilidadeRequest;
use App\Http\Controllers\Controller;

use App\Models\Sustentabilidade;

class SustentabilidadeController extends Controller
{
    public function index()
    {
        $registro = Sustentabilidade::first();

        return view('painel.sustentabilidade.edit', compact('registro'));
    }

    public function update(SustentabilidadeRequest $request, Sustentabilidade $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Sustentabilidade::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Sustentabilidade::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = Sustentabilidade::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = Sustentabilidade::upload_imagem_4();

            $registro->update($input);

            return redirect()->route('painel.sustentabilidade.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
