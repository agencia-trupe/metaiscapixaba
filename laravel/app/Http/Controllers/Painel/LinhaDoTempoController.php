<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\LinhaDoTempoRequest;
use App\Http\Controllers\Controller;

use App\Models\Registro;

class LinhaDoTempoController extends Controller
{
    public function index()
    {
        $registros = Registro::ordenados()->get();

        return view('painel.linha-do-tempo.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.linha-do-tempo.create');
    }

    public function store(LinhaDoTempoRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Registro::upload_imagem();

            Registro::create($input);

            return redirect()->route('painel.linha-do-tempo.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Registro $registro)
    {
        return view('painel.linha-do-tempo.edit', compact('registro'));
    }

    public function update(LinhaDoTempoRequest $request, Registro $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Registro::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.linha-do-tempo.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Registro $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.linha-do-tempo.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function deleteImage(Registro $registro)
    {
        try {

            $registro->update(['imagem' => '']);

            return redirect()->route('painel.linha-do-tempo.edit', $registro->id)->with('success', 'Imagem removida com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao remover imagem: '.$e->getMessage()]);

        }
    }

}
