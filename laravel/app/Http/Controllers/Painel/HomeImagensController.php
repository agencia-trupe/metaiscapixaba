<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\HomeImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\HomeImagem;

class HomeImagensController extends Controller
{
    public function index()
    {
        $registros = HomeImagem::ordenados()->get();

        return view('painel.home-imagens.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.home-imagens.create');
    }

    public function store(HomeImagensRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = HomeImagem::upload_imagem();

            HomeImagem::create($input);

            return redirect()->route('painel.home-imagens.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(HomeImagem $registro)
    {
        return view('painel.home-imagens.edit', compact('registro'));
    }

    public function update(HomeImagensRequest $request, HomeImagem $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = HomeImagem::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.home-imagens.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(HomeImagem $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.home-imagens.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
