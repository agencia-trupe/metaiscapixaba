<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EmpresaArquivosRequest;
use App\Http\Controllers\Controller;

use App\Models\EmpresaArquivo;

use App\Helpers\Tools;

class EmpresaArquivosController extends Controller
{
    public function index()
    {
        $registros = EmpresaArquivo::ordenados()->get();

        return view('painel.empresa-arquivos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.empresa-arquivos.create');
    }

    public function store(EmpresaArquivosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = EmpresaArquivo::upload_capa();
            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'assets/empresa-arquivos/');
            }

            EmpresaArquivo::create($input);

            return redirect()->route('painel.empresa-arquivos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(EmpresaArquivo $registro)
    {
        return view('painel.empresa-arquivos.edit', compact('registro'));
    }

    public function update(EmpresaArquivosRequest $request, EmpresaArquivo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = EmpresaArquivo::upload_capa();
            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'assets/empresa-arquivos/');
            }

            $registro->update($input);

            return redirect()->route('painel.empresa-arquivos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(EmpresaArquivo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.empresa-arquivos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
