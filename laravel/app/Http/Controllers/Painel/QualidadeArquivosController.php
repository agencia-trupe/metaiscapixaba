<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\QualidadeArquivosRequest;
use App\Http\Controllers\Controller;

use App\Models\QualidadeArquivo;
use App\Helpers\Tools;

class QualidadeArquivosController extends Controller
{
    public function index()
    {
        $registros = QualidadeArquivo::ordenados()->get();

        return view('painel.qualidade-arquivos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.qualidade-arquivos.create');
    }

    public function store(QualidadeArquivosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = QualidadeArquivo::upload_capa();
            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'assets/qualidade-arquivos/');
            }

            QualidadeArquivo::create($input);

            return redirect()->route('painel.qualidade-arquivos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(QualidadeArquivo $registro)
    {
        return view('painel.qualidade-arquivos.edit', compact('registro'));
    }

    public function update(QualidadeArquivosRequest $request, QualidadeArquivo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = QualidadeArquivo::upload_capa();
            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'assets/qualidade-arquivos/');
            }

            $registro->update($input);

            return redirect()->route('painel.qualidade-arquivos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(QualidadeArquivo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.qualidade-arquivos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
