<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EmpresaCertificacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\EmpresaCertificacao;

class EmpresaCertificacoesController extends Controller
{
    public function index()
    {
        $registros = EmpresaCertificacao::ordenados()->get();

        return view('painel.empresa-certificacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.empresa-certificacoes.create');
    }

    public function store(EmpresaCertificacoesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EmpresaCertificacao::upload_imagem();

            EmpresaCertificacao::create($input);

            return redirect()->route('painel.empresa-certificacoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(EmpresaCertificacao $registro)
    {
        return view('painel.empresa-certificacoes.edit', compact('registro'));
    }

    public function update(EmpresaCertificacoesRequest $request, EmpresaCertificacao $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = EmpresaCertificacao::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.empresa-certificacoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(EmpresaCertificacao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.empresa-certificacoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
