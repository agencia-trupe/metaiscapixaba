<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SejaNossoFornecedorRequest;
use App\Http\Controllers\Controller;

use App\Models\SejaNossoFornecedor;

class SejaNossoFornecedorController extends Controller
{
    public function index()
    {
        $registro = SejaNossoFornecedor::first();

        return view('painel.seja-nosso-fornecedor.edit', compact('registro'));
    }

    public function update(SejaNossoFornecedorRequest $request, SejaNossoFornecedor $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = SejaNossoFornecedor::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.seja-nosso-fornecedor.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
