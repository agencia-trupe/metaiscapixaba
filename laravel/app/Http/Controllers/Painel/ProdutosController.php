<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produtos;

class ProdutosController extends Controller
{
    public function index()
    {
        $registro = Produtos::first();

        return view('painel.produtos.edit', compact('registro'));
    }

    public function update(ProdutosRequest $request, Produtos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Produtos::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Produtos::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = Produtos::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = Produtos::upload_imagem_4();

            $registro->update($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
