<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\QualidadeRequest;
use App\Http\Controllers\Controller;

use App\Models\Qualidade;

class QualidadeController extends Controller
{
    public function index()
    {
        $registro = Qualidade::first();

        return view('painel.qualidade.edit', compact('registro'));
    }

    public function update(QualidadeRequest $request, Qualidade $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Qualidade::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Qualidade::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = Qualidade::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = Qualidade::upload_imagem_4();

            $registro->update($input);

            return redirect()->route('painel.qualidade.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
