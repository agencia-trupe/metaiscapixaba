<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');
    Route::get('produtos', 'ProdutosController@index')->name('produtos');
    Route::get('qualidade', 'QualidadeController@index')->name('qualidade');
    Route::get('sustentabilidade', 'SustentabilidadeController@index')->name('sustentabilidade');
    Route::get('seja-nosso-fornecedor', 'FornecedorController@index')->name('fornecedor');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato.post', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('linha-do-tempo', 'LinhaDoTempoController');
        Route::get('linha-do-tempo/{linha_do_tempo}/remover-imagem', 'LinhaDoTempoController@deleteImage')
            ->name('painel.linha-do-tempo.deleteImage');
		Route::resource('empresa-certificacoes', 'EmpresaCertificacoesController');
		Route::resource('empresa-arquivos', 'EmpresaArquivosController');
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('qualidade-arquivos', 'QualidadeArquivosController');
		Route::resource('qualidade', 'QualidadeController', ['only' => ['index', 'update']]);
		Route::resource('home-imagens', 'HomeImagensController');
		Route::resource('chamadas', 'ChamadasController');
		Route::resource('banners', 'BannersController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('seja-nosso-fornecedor', 'SejaNossoFornecedorController', ['only' => ['index', 'update']]);
		Route::resource('sustentabilidade', 'SustentabilidadeController', ['only' => ['index', 'update']]);
		Route::resource('produtos', 'ProdutosController', ['only' => ['index', 'update']]);
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
