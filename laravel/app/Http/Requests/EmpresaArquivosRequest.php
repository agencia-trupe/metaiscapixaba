<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaArquivosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'descricao' => 'required',
            'capa' => 'required|image',
            'arquivo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['arquivo'] = '';
        }

        return $rules;
    }
}
