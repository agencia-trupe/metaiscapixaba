<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SejaNossoFornecedorRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'frase' => 'required',
            'imagem' => 'image',
        ];
    }
}
