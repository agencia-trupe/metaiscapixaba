<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'texto' => 'required',
            'missao' => 'required',
            'visao' => 'required',
            'valores' => 'required',
        ];
    }
}
