<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QualidadeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'texto_1' => 'required',
            'texto_2' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
        ];
    }
}
