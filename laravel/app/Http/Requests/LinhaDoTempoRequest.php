<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LinhaDoTempoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'ano' => 'required|integer|min:1',
            'descricao' => '',
            'imagem' => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
